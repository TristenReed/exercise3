#include <fstream>
#include <iostream>
#include <conio.h>
#include <string>
using namespace std;

/*global variables are okay if its a const */ /*nobody can change them*/
const string path = "c:\\temp\\test.txt";

void PrintOutMadLib(string *madlib, ostream &os)
{
	os << "You and your " << madlib[0] << " friend " << madlib[1] << " were chilling at " << madlib[2] << " .\n You started to journey to the "
	<< madlib[3] << " but you only had " << madlib[4] << " minutes. \nAll of a sudden a " << madlib[5] << " hit. Mass panic and chaos began to erupt in the surrounding enviroment. \nThen a " <<
	madlib[6] << " came out of nowhere and crushed " << madlib[7] << ". \nYou stopped to take a closer look and the " << madlib[8] << " swarmed in and started " << madlib[9] << "until and then it was all over";
}

int main()
{
	bool loop = false;
	const int SIZE = 10;
	string madlib[SIZE];
	while (loop == false)
	{
		cout << "Enter an adjective <describing word>: \n ";
		cin >> madlib[0];
		cout << "Enter your friends name: \n ";
		cin >> madlib[1];
		cout << "Enter the name of a place: \n ";
		cin >> madlib[2];
		cout << "Enter your destination: \n ";
		cin >> madlib[3];
		cout << "Enter an integer between 1 and 10: \n ";
		cin >> madlib[4];
		cout << "Enter a natural disaster: \n ";
		cin >> madlib[5];
		cout << "Enter a vehicle model: \n ";
		cin >> madlib[6];
		cout << "Enter your type of animal: \n ";
		cin >> madlib[7];
		cout << "Enter a branch of military: \n ";
		cin >> madlib[8];
		cout << "Enter a verb: \n ";
		cin >> madlib[9];
		loop = true;
	}

	PrintOutMadLib(madlib, cout);

	char  input = 'n';
	cout << "Do you want to save the file?";
	cin >> input;

	if (input == 'y')
	{
		//ofstream ofs;
		//ofs.open(path);
		ofstream ofs(path);
		PrintOutMadLib(madlib, ofs);
		ofs.close();
	}

	//read in file
	/*ifstream ifs(path);
	string line;
	while (getline(ifs, line))
	{
	cout << line << "\n"
	}
	ifs.close();*/
	_getch();
	return 0;
}